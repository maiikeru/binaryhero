//
// Created by maiikeru on 31/10/15.
//

#ifndef BINARY_LIBRARY_H
#define BINARY_LIBRARY_H

#include "game.h"

/* The following header files need to be included in the C program. */
#include <termios.h>
#include <unistd.h>

/* For _kbhit() ONLY, these additional header files are also required. */
#include <sys/types.h>


/*
  Changes terminal input mode (helper function for kbhit)
  - Input: 0 if want line-by-line (buffered)
           1 if want immediate (non-buffered)
 - Returns: nothing
  Call _terminalmode(1) before using _kbhit.
  Call _terminalmode(0) after  using _kbhit.
*/
void _terminalmode(int dir);
/*
  Checks if any key was pressed
  - Execution is not blocked
  - Does not echo character to screen
 - Returns: 0 if no, non-zero if yes
 */
int _kbhit(void);
/*
  Reads a single typed character
  - Blocks until key is pressed
  - Pressing ENTER is not required
  - Does not echo character to screen
 - Returns: ASCII code of character
 */
int _getch(void);

/*
  Reads a single typed character
  - Blocks until key is pressed
  - Pressing ENTER is not required
  - Echoes character to the screen
  - Returns: ASCII code of character
 */
int _getche(void);
/*
  Clears the terminal screen
   - Works in Cygwin command shell
   - Works in NetBeans output window
 */
void clrscr(void);

#endif //BINARY_LIBRARY_H
