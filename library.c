//
// Created by maiikeru on 31/10/15.
//

#include "library.h"

/* The following header files need to be included in the C program. */
#include <termios.h>
#include <unistd.h>

/* For _kbhit() ONLY, these additional header files are also required. */
#include <sys/types.h>


void _terminalmode(int dir) {
    static struct termios oldt, newt;

    if (dir == 1) {
        /* Terminal operates in noncanonical mode (input available immediately) */
        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        newt.c_lflag &= ~(ICANON | ECHO);
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    }
    else
        /* Terminal operates in canonical mode (input available line-by-line) */
        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}


int _kbhit(void) {
    struct timeval tv;
    fd_set rdfs;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rdfs);
    FD_SET (STDIN_FILENO, &rdfs);

    select(STDIN_FILENO + 1, &rdfs, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &rdfs);
}


int _getch(void) {
    struct termios oldattr, newattr;
    int ch;

    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON | ECHO);

    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);

    return ch;
}


int _getche(void) {
    struct termios oldattr, newattr;
    int ch;

    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON);

    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);

    return ch;
}


void clrscr(void) {
    printf("\033[H\033[J");
}