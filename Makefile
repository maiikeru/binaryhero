##
# Projekt: Binary
# Branch:  Master
# Autor:   Michal Jurca
# Datum:   23.9.2015  
# Změna:   23.9.2015  
# Použití:
#	-překlad     make
#	-smazat      make clean
#	-zabalit     make pack                  
##  

###############################################################################
#  Parameters
###############################################################################

# Jmeno archivu a soubory
PACK_NAME := mjurca_semestralni_ukol_I

#Smazani souboru
RM = rm -f

#Compilator	
CPP:=gcc

# Optimalizace
OPT = -g -O3 

# Parametry a compilator pro C++
FLAGS =-std=gnu99 -Wall -Werror -pedantic-errors $(OPT)
#FLAGS =-std=c99 -Wall -Werror -pedantic-errors $(OPT)
#FLAGS =  -std=c++11 

# Zdrojový soubor
SRCS = $(wildcard *.c *.h)

# Objektový soubor
OBJFILES = $(SRCS:.c.h=.o)

# Spustitelný soubor
EXEFILE:= main

# Obsah projektu
ALLFILES =  Makefile $(SRCS) 


###############################################################################
#  Compilation
###############################################################################

.PHONY: clean pack 

all: $(EXEFILE)

library.o: library.c library.h game.c game.h
	$(CPP) -c $(FLAGS) $< -o $@	
	
game.o: game.c game.h library.h library.c
	$(CPP) -c $(FLAGS) $< -o $@

main.o: main.c game.h
	$(CPP) -c $(FLAGS) $< -o $@
		
$(EXEFILE): $(OBJFILES)
	$(CPP) $(FLAGS) $(OBJFILES) -o $@
###############################################################################
#  Cleaning and packing
###############################################################################

clean:
	$(RM) $(OBJFILES) $(EXEFILE) *~

# Vytvoreni mjurca_semestralni_ukol_I.zip
pack:
	zip $(PACK_NAME).zip $(ALLFILES)


#debug: $(EXEFILE)
#	export XEDITOR=gvim;ddd $(EXEFILE)


run: $(EXEFILE)
	./$(EXEFILE)

# End of Makefile
