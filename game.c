//
// Created by maiikeru on 30/10/15.
//


#include "game.h"


int RandomNum() {
    //Inicializace pseudo generatoru cisel
    time_t t;
    srand((unsigned) time(&t));

    //generace cisel 0-15
    return rand() % 16;
}

void DecToBin(int num, char *convert) {
    int i;
    convert[5] = '\0';
    for (i = 4; i >= 0; --i, num >>= 1)
        convert[i] = (char) ((num & 1) + '0');
}

void Inicialization(Game *pGame) {

    pGame->life = 3;
    pGame->killPos = 0;
    pGame->sleep = 1000000 - pGame->level * 50000;

    // vynulovani pole
    int i, j;
    for (i = 0; i < 12; ++i) {
        for (j = 0; j < 4; ++j) {
            pGame->maps[i][j] = ' ';
        }
    }

    // vygenerovane cislo
    int num = RandomNum();
    // prevedeni cisla do binarky
    DecToBin(num, pGame->binary);

    // prevedeni na hexadecimal
    num < 10 ? (num += 48) : (num += 55);

    pGame->maps[0][0] = num;
    pGame->maps[0][1] = '#';
    pGame->maps[0][2] = '#';
    pGame->maps[0][3] = '#';
    pGame->maps[10][0] = '^';
    pGame->maps[11][0] = '-';
    pGame->maps[11][1] = '-';
    pGame->maps[11][2] = '-';
    pGame->maps[11][3] = '-';
}

void PrintMat(Game *pGame) {
    clrscr();

    printf("\n\n\n\n\n                                C Console Binary Hero - ASCII Game     \n\n\n");
    printf("                                Life: %d, Level: %d, Score: %d \n\n", pGame->life, pGame->level,
           pGame->score);

    int i, j;
    for (i = 0; i < 12; ++i) {
        printf("\t\t\t\t");
        for (j = 0; j < 4; ++j) {
            printf("%c\t", pGame->maps[i][j]);
        }
        printf("\n\n");
    }
    printf("\n");
}

void stepKill(Game *pGame, int position) {
    pGame->maps[10][pGame->killPos] = ' ';
    pGame->killPos = position;
    pGame->maps[10][pGame->killPos] = '^';
}

bool PressedButton(Game *pGame) {

    int press;
    _terminalmode(1);
    if (_kbhit()) {
        press = _getch();
        switch (press) {
            // Posunuti do leva
            case 'a':
                stepKill(&(*pGame), 0);
                break;
            case 's':
                stepKill(&(*pGame), 1);
                break;
            case 'd':
                stepKill(&(*pGame), 2);
                break;
            case 'f':
                stepKill(&(*pGame), 3);
                break;

            case 'k':
                StepLeft(pGame);
                break;
                // Posunuti do prava

            case 'l':
                StepRight(pGame);
                break;
                // Ukonceni hry
            case 'q':
                _terminalmode(0);
                PrintQuit();
                return true;
            case 'h':
                PrintHelp(pGame);
                // Tiskni ziskany znak
            default:
                break;
        }
        fflush(stdout);
    }
    _terminalmode(0);
    return false;
}

void GenBinNum(Game *pGame) {
    //Inicializace generatoru cisel
    time_t t;
    srand((unsigned) time(&t));

    //generace cisel 0-15
    int position = rand() % 4;
    int num = rand() % 4;

    if (num > 1)
        pGame->maps[1][position] = 32;
    else
        pGame->maps[1][position] = num + 48;

}

void MakeStep(Game *pGame) {

    printf("\n");// delete
    int i, j, k;
    for (i = 10; i > 0; i--) {
        for (j = 0; j < 4; ++j) {
            // prace jenom s 1 nebo 0
            if (pGame->maps[i][j] == 48 || pGame->maps[i][j] == 49) {
                // dalsi krok
                k = i + 1;
                if (k == 11) {
                    if (((pGame->maps[k][j] == '-') || (pGame->maps[k][j] == 120)) &&
                        (pGame->maps[i][j] == pGame->binary[j + 1]))
                        pGame->score += 10;

                    if (pGame->maps[i][j] != pGame->binary[j + 1]) {
                        // ubrat zivot
                        if (pGame->life > 0) {
                            pGame->maps[0][pGame->life] = ' ';
                            pGame->life -= 1;
                        }
                        // chyba nastavit x
                        pGame->maps[k][j] = 120;
                    }
                    else
                        pGame->maps[k][j] = pGame->maps[i][j];
                }
                else {
                    // jestli to neni zabijak tak presun do dalsi bunky
                    if (pGame->maps[k][j] != '^')
                        pGame->maps[k][j] = pGame->maps[i][j];
                }
                //aktualni bunku prepis
                pGame->maps[i][j] = 32;
            }
        }
    }
}

void StepLeft(Game *pGame) {

    // posunuti ukazatele v levo, v pripade 0 se presune na druhou stanu
    if (pGame->killPos > 0) {
        pGame->maps[10][pGame->killPos] = ' ';
        pGame->killPos -= 1;
        pGame->maps[10][pGame->killPos] = '^';
    }
    else {
        pGame->maps[10][pGame->killPos] = ' ';
        pGame->killPos = 3;
        pGame->maps[10][pGame->killPos] = '^';
    }
}

void StepRight(Game *pGame) {

    // posunuti ukazatele v pravo, v pripade 3 se presune na druhou stanu
    if (pGame->killPos < 3) {
        pGame->maps[10][pGame->killPos] = ' ';
        pGame->killPos += 1;
        pGame->maps[10][pGame->killPos] = '^';
    }
    else {
        pGame->maps[10][pGame->killPos] = ' ';
        pGame->killPos = 0;
        pGame->maps[10][pGame->killPos] = '^';
    }
}

int GameStatus(Game *pGame, Backup pBackup[10], int i) {

    if (pGame->life <= 0) {
        clrscr();
        printf("\n\n\n\n\n                                C Console Binary Hero - ASCII Game     \n");
        printf("\n\n                                Game OVER \n\n");
        _getch();

        if (i >= 10)
            i = 10;

        memcpy(pBackup[i].player, pGame->player, sizeof(pGame->player));
        pBackup[i].level = pGame->level;
        pBackup[i].score = pGame->score;
        pBackup[i].life = pGame->life;
        pBackup[i].pos = i;

        pGame->level = 0;
        pGame->score = 0;
        return 2;
    }
    else {
        int j = 0;
        for (; j < 4; j++) {
            if (pGame->maps[11][j] != pGame->binary[j + 1])
                return 0;
        }

        clrscr();
        printf("\n\n\n\n\n                                C Console Binary Hero - ASCII Game     \n");
        printf("\n\n                                Game WIN \n");
        printf("\n\n\n\n\n                                Press any key to continue next level     \n\n");
        _getch();

        // nastaveni rychlosti a levlu
        pGame->sleep = 1000000 - pGame->level * 50000;
        pGame->level += 1;
    }

    return 1;
}


void PrintHelp(Game *pGame) {
    clrscr();

    if (pGame->life > 0) {
        pGame->maps[0][pGame->life] = ' ';
        pGame->life -= 1;
    }

    printf("\n\n\n\n\n                                C Console Binary Hero - ASCII Game     \n\n\n");
    printf("                                Life: %d, Level: %d, Score: %d \n\n", pGame->life, pGame->level,
           pGame->score);

    int i, j, k;
    for (i = 0; i < 11; ++i) {
        printf("\t\t\t\t");
        for (j = 0; j < 4; ++j) {
            printf("%c\t", pGame->maps[i][j]);
        }
        printf("\n\n");
    }
    printf("\t\t\t\t");
    for (k = 1; k < 5; k++) {
        printf("%c\t", pGame->binary[k]);
    }
    printf("\n\n\n");

    sleep(3);
}

void PrintQuit() {
    clrscr();
    printf("\n\n\n\n\n                                C Console Binary Hero - ASCII Game     \n");
    printf("\n\n                                Game QUIT \n");
}

void SortPlayerList(Backup *p, int velikostPole) {

    bool seradit = true;
    while (seradit) {
        seradit = false;
        for (int i = 0; i < (velikostPole - 1); i++) {
            if ((*(p + i)).level < (*(p + i + 1)).level) {
                seradit = true;
                Backup temp = (*(p + i));
                (*(p + i)) = (*(p + i + 1));
                (*(p + i + 1)) = temp;
            }
            else if (((*(p + i)).level == (*(p + i + 1)).level) && ((*(p + i)).life < (*(p + i + 1)).life)) {
                seradit = true;
                Backup temp = (*(p + i));
                (*(p + i)) = (*(p + i + 1));
                (*(p + i + 1)) = temp;
            }
        }
    }
}

void PrintPlayer(Backup *p, int velikostPole) {

    FILE *f;

    if ((f = fopen("file.txt", "w")) == NULL) {
        fprintf(stderr, "Nelze zapisocat do souboru!");
        return;
    }

    for (int i = 0; i <= (velikostPole); i++) {
        printf("\t\t\t\tName: %s, Level: %d, Life: %d, Score: %d\n", (*(p + i)).player, (*(p + i)).level,
               (*(p + i)).life,
               (*(p + i)).score);
        fprintf(f, ":%s:%d:%d:%d*\n", (*(p + i)).player, (*(p + i)).level, (*(p + i)).life,
                (*(p + i)).score);
    }

    fclose(f);
    printf("\n\n\n\n\n                                Press any key to play     \n\n");
    _getch();
}