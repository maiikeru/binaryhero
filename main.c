#include "game.h"




void PrintMenuSetting() {
// vymazani obrazovky
    clrscr();
    printf("\n\t\t   C Console BiNaRy HeRo - ASCII Game \n\n");

    printf("\t\t-----------------------------------------\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t|\t\tSetting\t\t\t|\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t-----------------------------------------\n\n\n");

    printf("\t\tControl KEYS: \n");
    printf("\t\t  a - cell 1\n");
    printf("\t\t  s - cell 2\n");
    printf("\t\t  d - cell 3\n");
    printf("\t\t  f - cell 4\n");
    printf("\t\t  h - help for a life\n");
    printf("\t\t  q - quit game\n");
    printf("\t\tBonus:\n");
    printf("\t\t  l - right\n");
    printf("\t\t  k - left\n");
    printf("\n\nPress any key to back Menu\n\n");
    _getch();
}

void PrintMenuCredits() {
    // vymazani obrazovky
    clrscr();
    printf("\n\t\t   C Console BiNaRy HeRo - ASCII Game \n\n");

    printf("\t\t-----------------------------------------\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t|\t\tHelp\t\t\t|\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t-----------------------------------------\n\n\n");

    printf("\tHra Binary Hero byla vytvorena autorem: Michal Jurca\n");
    printf("\tHra generuje hexadecimalni cislo, ktere je prevedeno do birniho tvaru\n");
    printf("\ta hrac ma za ukol dane cislo slozit. \n");


    printf("\n\nPress any key to back Menu\n\n");
    _getch();
}

void PrintMenuNewGame() {
    // vymazani obrazovky
    clrscr();
    printf("\n\t\t   C Console BiNaRy HeRo - ASCII Game \n\n");

    printf("\t\t-----------------------------------------\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t|\t\tNew Game\t\t|\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t-----------------------------------------\n\n\n");

    printf("\n\nPress any key to back Menu\n\n");

    _getch();
}

void MainMenu(int *index, bool enter, bool *game, bool *startGame) {

    // vymazani obrazovky
    clrscr();

    // osetreni
    if (*index < 0) { *index = 0; }
    if (*index > 4) { *index = 4; }

    printf("\n\t\t   C Console BiNaRy HeRo - ASCII Game \n\n");
    printf("\t\t-----------------------------------------\n");
    printf("\t\t|\t\t\t\t\t|\n");

    *index == 0 ? printf("\t\t|\t\t-> New Game\t\t|\n") : printf("\t\t|\t\tNew Game\t\t|\n");
    if ((*index == 0) && enter) {
        *game = false;
        *startGame = true;
    }
    printf("\t\t|\t\t\t\t\t|\n");

    *index == 2 ? printf("\t\t|\t\t-> Setting\t\t|\n") : printf("\t\t|\t\tSetting\t\t\t|\n");
    if ((*index == 2) && enter) { PrintMenuSetting(); }
    printf("\t\t|\t\t\t\t\t|\n");

    *index == 3 ? printf("\t\t|\t\t-> Help\t\t\t|\n") : printf("\t\t|\t\tHelp\t\t\t|\n");
    if ((*index == 3) && enter) { PrintMenuCredits(); }
    printf("\t\t|\t\t\t\t\t|\n");

    *index == 4 ? printf("\t\t|\t\t-> Quit\t\t\t|\n") : printf("\t\t|\t\tQuit\t\t\t|\n");
    if ((*index == 4) & enter) { *game = false; }
    printf("\t\t-----------------------------------------\n");

    printf("\t\t H -> UP, j -> DOWN\n");
}

bool MenuRun() {

    int index = 0;
    bool game = true;
    bool startGame = false;
    int press;
    while (game) {
        MainMenu(&index, false, &game, &startGame);
        _terminalmode(1);
        if (_kbhit()) {
            press = _getch();

            printf("%c", press);
            switch (press) {
                //enter
                case 10:
                case 13:
                    MainMenu(&(index), true, &game, &startGame);
                    _terminalmode(0);
                    break;
                    // q, ecs
                case 113:
                case 81:
                    _terminalmode(0);
                    game = false;
                    break;
                    // h
                case 104:
                    index--;
                    MainMenu(&(index), false, &game, &startGame);
                    break;
                    // j
                case 106:
                    index++;
                    MainMenu(&(index), false, &game, &startGame);
                    break;
                default:
                    break;
            }
        }
        fflush(stdout);
        _terminalmode(0);
        usleep(2000);
    }

    clrscr();
    return startGame;
}

bool GetPlayerName(Game *pGame) {

    // vymazani obrazovky
    clrscr();
    printf("\n\t\t   C Console BiNaRy HeRo - ASCII Game \n\n");

    printf("\t\t-----------------------------------------\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t|\t\tSet player name\t\t|\n");
    printf("\t\t|\t\t\t\t\t|\n");
    printf("\t\t-----------------------------------------\n\n\n");

    printf("Enter the player name:\n");
    printf("-> ");


    int count;
    while ((count = scanf("%99s", pGame->player)) != 1) {
        if (count < 0)
            return false;
        // zahozeni balastu
        scanf("%*c");
        fflush(stdin);
        printf("Try again.\n");
    }
    printf("Player name is %s\n", pGame->player);

    usleep(50);

    return true;
}

int main(void) {

    // Inicializace struktury
    Game g;

    Backup B[10];
    int status, i =0;
    bool over = false;
    g.level = 0;
    g.score = 0;
    bool viewMenu = true;

    while (!over) {

        if (viewMenu) {
            if (!MenuRun()) { return EXIT_FAILURE; }
            GetPlayerName(&g);
            viewMenu = false;


        }

        Inicialization(&g);
        PrintMat(&g);

        while (!(over = PressedButton(&g))) {
            GenBinNum(&g);
            PrintMat(&g);
            MakeStep(&g);
            PrintMat(&g);

            status = GameStatus(&g, B, i);
            if (status == 1)
                break;
            else if (status == 2) {

                SortPlayerList(B, i+1);
                PrintPlayer(B, i);
                clrscr();
                i++;
                viewMenu = true;
                break;
            }

            usleep(g.sleep);
        }
    }

    return 0;
}
