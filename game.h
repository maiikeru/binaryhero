//
// Created by maiikeru on 30/10/15.
//

#ifndef BINARY_GAME_H
#define BINARY_GAME_H

#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "library.h"
#include <string.h>

typedef struct game {
    int maps[12][4];
    int killPos;
    int life;
    unsigned sleep;
    unsigned level;
    // max 32 bit cislo
    char binary[33];
    int score;
    char player[100];
} Game;


typedef struct backup {
    int life;
    unsigned level;
    char player[100];
    int score;
    int pos;
} Backup;


int RandomNum();

void DecToBin(int num, char *convert);

void Inicialization(Game *pGame);

void PrintMat(Game *pGame);

bool PressedButton(Game *pGame);

void GenBinNum(Game *pGame);

void MakeStep(Game *pGame);

void StepLeft(Game *pGame);

void StepRight(Game *pGame);


int GameStatus(Game *pGame, Backup pBackup[10], int i);


void PrintHelp(Game *pGame);

void PrintQuit();


void SortPlayerList(Backup *p, int i);

void PrintPlayer(Backup *p, int i);


#endif //BINARY_GAME_H
